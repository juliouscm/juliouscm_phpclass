<?php
	include 'array4.php';
	if (isset($_GET) && isset($_GET['search'])) {
		$name = $_GET['name'];
		if (($res = array_search($name, $nombres)) !== false) {
			echo $nombres[$res].' encontrado: indice '.$res;
		}
		else{
			$encontrado = false;
			foreach ($nombres as $indice => $nombre) {
				if (strtolower($name) == strtolower($nombre)) {
					$encontrado = true;
					echo $name.' encontrado: indice '.$indice;
					break;
				}
			}
			if(!$encontrado)
				echo $name.' no encontrado :(';
		}
	}
?>
<form>
	<input type="text" name="name" placeholder="Entre Nombre">
	<input type="submit" name="search" value="Go">
</form>