<?php
	if(isset($_POST) && isset($_POST['guardar'])){
		if(trim($_POST['nombre']) !== '' && trim($_POST['apellido']) !== '' && $_POST['email'] !== ''){
			//Guardar datos de usuario
			unset($_POST['guardar']);
			file_put_contents('clientes.csv', implode(',', $_POST)."\n",FILE_APPEND);
			header('Location: '.$_SERVER['PHP_SELF'].'?completado');
		}
		else{
			echo "Error: Verifique - ";
			echo (trim($_POST['nombre']) == '')?'Nombre en blanco ':'';
			echo (trim($_POST['apellido']) == '')?'Apellido en blanco ':'';
			echo ($_POST['email'] == '')?'Email en blanco ':'';
		}
	}
	if(isset($_GET['completado'])){
		echo "<h3>Guardado</h3>";
	}
?>
<form method="post">
	<input type="text" name="nombre" placeholder="Entre Nombre" required><br/>
	<input type="text" name="apellido" placeholder="Entre Apellido" required><br/>
	<input type="email" name="email" placeholder="Entre Email" required><br/>
	<input type="text" name="direccion" placeholder="Direccion(Opcional)"><br/>
	<input type="tel" name="tel" placeholder="Telefono(Opcional)"><br/>
	<input type="submit" name="guardar" value="Guardar">
</form>