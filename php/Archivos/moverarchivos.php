<?php
	if (!is_dir('php') || !is_dir('html')) {
		die('Carpeta no existe');
	}
	$dir = scandir('.');
	foreach ($dir as $key => $value) {
		if (is_dir($value)) continue;
		$ext = explode('.', $value);
		$count = count($ext)-1;
		$extexist = (isset($ext[$count]) && $count)?true:false;
		if($extexist){
			$ext = strtolower($ext[$count]);
			if(!is_dir($ext)) continue;
			if('/'.$value == $_SERVER['PHP_SELF']) continue;
			if(rename($value, $ext.'/'.$value)){
				echo $value.' ...movido<br/>';
			}
			else{
				echo $value.' ...<b>NO</b> movido<br/>';
			}
		}
	}
	$dir = scandir('.');
	$arr = array();
	foreach ($dir as $value) {
		if(!is_dir($value)) continue;
		$arr[] = $value;
	}
	$dif = array_diff($dir, $arr);
	echo implode('<br/>', $dif);
?>