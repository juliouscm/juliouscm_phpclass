<?php
	$v1 = 10;
	$v2 = 8;

	if ($v1 == $v2) {
		echo $v1.' es igual a '.$v2.'<br/>';
	}

	if ($v1 < $v2) {
		echo $v1.' es menor que '.$v2.'<br/>';
	}

	if ($v1 >= $v2) {
		echo $v1.' es mayor o igual a '.$v2.'<br/>';
	}

	//if else
	if ($v1 == $v2) {
		echo 'Esto no se cumple';
	} else {
		echo 'No son iguales<br/>';
	}
	
	//if else elseif
	if ($v1 == $v2) {
		echo 'Es igual<br/>';
	}
	elseif ($v1 > $v2) {
		echo 'Es mayor<br/>';
	}
	else{
		echo 'No se cumplio<br/>';
	}

	//Comparacion de valores
	if (18 > 4) {
		echo '18 es mayor que 4<br/>';
	}

	if ('10' == 10) {
		echo "'10' es igual 10<br/>";
	}
	if ('10' === 10) {
		echo "'10' es identico a 10<br/>";
	}

	if (!(10 > 8)) {
		echo '10 es mayor que 8<br/>';
	}

	if ((10 > 8) && (8 < 10)) {
		echo '& la condicion se cumple<br/>';
	}

	if ((10 > 8) || (8 > 10)) {
		echo 'Or la condicion se cumple<br/>';
	}
	if ((10 > 8) || (8 > 10) && (8 == 10)) {
		echo 'Or la condicion se cumple<br/>';
	}
	if ((10 > 8) || (8 > 10)) {
		echo 'Or la condicion se cumple<br/>';
	}
	if ((10 > 8) || (8 > 10)) {
		echo 'Or la condicion se cumple<br/>';
	}
?>





