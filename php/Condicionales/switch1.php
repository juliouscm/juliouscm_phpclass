<form>
	<select name="mascota">
		<option>Seleccione su mascota favorita</option>
		<option value="perro">Perro</option>
		<option value="gato">Gato</option>
		<option value="raton">Raton</option>
		<option value="perico">Perico</option>
	</select>
	<input type="submit" name="enviar" value="Enviar">
</form>
<?php
	if (isset($_GET['mascota'])) {
		switch ($_GET['mascota']) {
			case 'perro':
				echo 'Woof';
				break;
			case 'gato':
				echo 'Miau';
				break;
			case 'raton':
				echo 'Squik';
				break;
			case 'perico':
				echo 'Twit';
				break;
			default:
				echo 'No ha seleccionado mascota';
				break;
		}
	}
?>