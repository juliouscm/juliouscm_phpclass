<?php
	//Desplegar errores
	ini_set('display_errors', 1);
	//Verificar datos entrados por usuarios
	if (isset($_GET['name']) && isset($_GET['surname']) && $_GET['name'] != '' && $_GET['surname'] != '') {
		//Desplegar datos de usuarios
		echo $_GET['name'].' '.$_GET['surname'];
		//Borramos datos del GET
		unset($_GET);
	}
	//Default
	elseif (!isset($_GET) || !count($_GET)) {
		echo 'Escriba su nombre y apellido';
	}
	//Errores
	else{
		echo 'Error';
		//Nombre de usuario valido
		if (isset($_GET['name']) && $_GET['name'] != '') {
			echo ': escriba apellido';
		}
		//Nombre de usuario no valido
		else{
			echo ': escriba nombre';
		}
	}
?>
<form>
	<input type="text" name="name" value="<?php if(isset($_GET['name'])) echo $_GET['name']; ?>">
	<input type="text" name="surname" value="<?php if(isset($_GET['surname'])) echo $_GET['surname']; ?>">
	<input type="submit">
</form>
<a href="<?php echo $_SERVER['PHP_SELF']; ?>">Reset</a>